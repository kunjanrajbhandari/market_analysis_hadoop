package market;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
  List<Item> itemList = new ArrayList<>();
  
  public void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
    int sum = 0;
    for (IntWritable value : values)
      sum += value.get(); 
    Item item = new Item(key.toString(), sum);
    this.itemList.add(item);
  }
  
  public void cleanup(Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
    Collections.sort(this.itemList);
    System.out.println(this.itemList);
    for (Item item : this.itemList)
      context.write(new Text(item.getName()), new IntWritable(item.getOccurance())); 
  }
}
