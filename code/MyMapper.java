package market;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
  MyMapper() {
    System.out.println("My mapper");
  }
  
  public void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
    String[] data = value.toString().split(",");
    String items = data[data.length - 1];
    String[] itemarray = items.split(":");
    for (int i = 0; i < itemarray.length; i++) {
      for (int j = i + 1; j < itemarray.length; j++) {
        String word1 = itemarray[i];
        String word2 = itemarray[j];
        if (word2.compareTo(word1) > 0) {
          context.write(new Text(String.valueOf(word2) + "," + word1), new IntWritable(1));
        } else {
          context.write(new Text(String.valueOf(word1) + "," + word2), new IntWritable(1));
        } 
      } 
    } 
  }
}
