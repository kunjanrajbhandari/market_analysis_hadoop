package market;

public class Item implements Comparable<Item> {
  private String name;
  
  private int occurance;
  
  public String getName() {
    return this.name;
  }
  
  public int getOccurance() {
    return this.occurance;
  }
  
  public int compareTo(Item item2) {
    return -(this.occurance - item2.occurance);
  }
  
  public Item(String name, int occurance) {
    this.name = name;
    this.occurance = occurance;
  }
  
  public String toString() {
    return "Item [name = " + this.name + ",occurance = " + this.occurance + "]";
  }
}
